Version 4
SymbolType BLOCK
RECTANGLE Normal -32 -40 32 40
WINDOW 0 0 -40 Bottom 2
WINDOW 3 0 40 Top 2
SYMATTR Prefix X
SYMATTR Value Si2302CDS
SYMATTR ModelFile C:\Users\Public\Documents\Altium\Projects\testpulse-theta\sim\model\si2302cds_ps.txt
PIN 32 -16 RIGHT 8
PINATTR PinName D
PINATTR SpiceOrder 1
PIN -32 0 LEFT 8
PINATTR PinName G
PINATTR SpiceOrder 2
PIN 32 16 RIGHT 8
PINATTR PinName S
PINATTR SpiceOrder 3
